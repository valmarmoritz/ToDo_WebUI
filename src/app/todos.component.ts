import { Component } from '@angular/core';
import { Todo } from './todo';
import { TodoService } from './todo.service';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'my-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css'],
  //providers: [TodoService]
})

export class TodosComponent implements OnInit {
  todos: Todo[];
  selectedTodo: Todo;

  constructor(private todoService: TodoService, private router: Router) { };

  ngOnInit(): void {
    this.getTodos();
  }

  getTodos(): void {
    this.todoService.getTodos()
      //.then(todos => this.todos.sort((a,b) => { return a.id - b.id; }))
      .then(todos => this.todos = todos)
      .then(todos => this.todos.sort((a,b) => { return a.id - b.id; }))
      ;
    //this.todoService.getTodosSlowly().then(todos => this.todos = todos);
  }

  onSelect(todo: Todo): void {
    this.selectedTodo = todo;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedTodo.id]);
  }

  add(asi: string): void {
    asi = asi.trim();
    if (!asi) { return; }
    this.todoService.create(asi)
      .then(todo => {
        this.todos.push(todo);
        this.selectedTodo = null;
      });
  }

  delete(todo: Todo): void {
    this.todoService
      .delete(todo.id)
      .then(() => {
        this.todos = this.todos.filter(h => h != todo);
        if (this.selectedTodo === todo) { this.selectedTodo = null; }
      });

      // kui kustutad viimase, saad kõik 'vaikimisi' asjad tagasi !
      // kui lubada kõik asjad kustutada, tuleb muuta Web API loogikat, sest see populeerib tühja nimekirja niikuinii
      if (this.todos.filter(h => h !=todo).length === 0) this.getTodos();  
  }

}

