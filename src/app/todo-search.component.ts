import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

// Observable class extensions
import 'rxjs/add/observable/of';

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { TodoSearchService } from './todo-search.service';
import { Todo } from './todo';

@Component({
    selector: 'todo-search',
    templateUrl: './todo-search.component.html',
    styleUrls: ['./todo-search.component.css'],
    providers: [TodoSearchService]
})
export class TodoSearchComponent implements OnInit {
    todos: Observable<Todo[]>;
    private searchTerms = new Subject<string>();

    constructor(
        private todoSearchService: TodoSearchService,
        private router: Router) { }

    // Push a search term into the observable stream.
    search(term: string): void {
        this.searchTerms.next(term);
    }

    ngOnInit(): void {
        this.todos = this.searchTerms
            .debounceTime(300)        // wait 300ms after each keystroke before considering the term
            .distinctUntilChanged()   // ignore if next search term is same as previous
            .switchMap(term => term   // switch to new observable each time the term changes
                // return the http search observable
                ? this.todoSearchService.search(term)
                // or the observable of empty todos if there was no search term
                : Observable.of<Todo[]>([]))
            .catch(error => {
                // TODO: add real error handling
                console.log(error);
                return Observable.of<Todo[]>([]);
            });
    }

    gotoDetail(todo: Todo): void {
        let link = ['/detail', todo.id];
        this.router.navigate(link);
    }
}