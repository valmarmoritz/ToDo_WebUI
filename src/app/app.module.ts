import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { TodosComponent } from './todos.component';
import { TodoService } from './todo.service';
import { TodoDetailComponent } from './todo-detail.component';
import { DashboardComponent } from './dashboard.component';
import { TodoSearchComponent } from './todo-search.component';

import { AppRoutingModule } from './app-routing.module';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule, // <-- import the FormsModule before binding with [(ngModel)]
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService, {
      passThruUnknownUrl: true
    }),
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    TodosComponent,
    TodoDetailComponent,
    TodoSearchComponent
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})

export class AppModule { }
