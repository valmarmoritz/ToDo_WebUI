"use strict";
var InMemoryDataService = (function () {
    function InMemoryDataService() {
    }
    InMemoryDataService.prototype.createDb = function () {
        var todos = [
            //{ id: 0, name: 'null' },
            { id: 11, isComplete: false, name: 'jaluta koera' },
            { id: 12, isComplete: false, name: 'käi poes' },
            { id: 13, isComplete: false, name: 'korista oma tuba ära' },
            { id: 14, isComplete: false, name: 'õpi Angular selgeks' },
            { id: 15, isComplete: true, name: 'käi saunas' },
            { id: 16, isComplete: false, name: 'otsi talvesaapad üles' },
            { id: 17, isComplete: false, name: 'vii vanaema Pärnusse' },
            { id: 18, isComplete: true, name: 'harjuta esinemist' },
            { id: 19, isComplete: false, name: 'uuenda oma LinkedIni profiili' },
            { id: 20, isComplete: false, name: 'pese mustad nõud ära' }
        ];
        return { todos: todos };
    };
    return InMemoryDataService;
}());
exports.InMemoryDataService = InMemoryDataService;
//# sourceMappingURL=in-memory-data.service.js.map