import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Todo } from './todo';

@Injectable()
export class TodoSearchService {

    constructor(private http: Http) { }

    search(term: string): Observable<Todo[]> {
        return this.http
            //.get(`api/todos/?name=${term}`)
            .get(`http://localhost:5000/api/todo/?name=${term}`)
            .map(response => response.json().data as Todo[]);
    }
}