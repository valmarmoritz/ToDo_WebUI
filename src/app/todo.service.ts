import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Todo } from './todo';

@Injectable()
export class TodoService {
    //private todosUrl = 'api/todos';  // URL to web api
    //private todosUrl = 'http://localhost:5000/api/todo';  // URL to web api
    private todosUrl = 'http://localhost:5000/api/todo';  // URL to web api

    constructor(private http: Http) { }

    getTodos(): Promise<Todo[]> {
        return this.http.get(this.todosUrl)
            .toPromise()
            //.then(response => response.json().data as Todo[])
            .then(response => response.json() as Todo[])
            .catch(this.handleError);
    }
    
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    getTodosSlowly(): Promise<Todo[]> {
        return new Promise(resolve => {
            // Simulate server latency with 2 second delay
            setTimeout(() => resolve(this.getTodos()), 2000);
        });
    }
    getTodo(id: number): Promise<Todo> {
        const url = `${this.todosUrl}/${id}`;
        return this.http.get(url)
            .toPromise()
            //.then(response => response.json().data as Todo)
            .then(response => response.json() as Todo)
            .catch(this.handleError);
        //return this.getTodos()
        //    .then(todos => todos.find(todo => todo.id === id));
    }

    private headers = new Headers({ 'Content-Type': 'application/json' });

    update(todo: Todo): Promise<Todo> {
        const url = `${this.todosUrl}/${todo.id}`;
        return this.http
            .put(url, JSON.stringify(todo), { headers: this.headers })
            .toPromise()
            .then(() => todo)
            .catch(this.handleError);
    }

    create(_name: string): Promise<Todo> {
        //var obj = { name: name };
        var obj = { "name": _name, "isComplete": false };
        return this.http
            .post(this.todosUrl, JSON.stringify(obj), { headers: this.headers })
            .toPromise()
            //.then(res => res.json().data as Todo)
            .then(res => res.json() as Todo)
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        const url = `${this.todosUrl}/${id}`;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }
}
