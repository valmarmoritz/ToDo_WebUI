import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    styleUrls: ['./app.component.css'],
    template: `
    <h1>{{title}}</h1>
    <nav>
        <a routerLink="/dashboard" routerLinkActive="active">Töölaud</a>
        <a routerLink="/todo" routerLinkActive="active">Todo nimekiri</a>
    </nav>
    <router-outlet></router-outlet>
    `
})
export class AppComponent {
    title = "ToDo mänedžer";
};